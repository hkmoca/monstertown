//
//  main.swift
//  MonsterTown
//
//  Created by Héctor Moreno on 12/06/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation

print("Hello, World!")

var myTown = Town(population: 0, stoplights: 6)
let myTownSize = myTown?.townSize
print(myTownSize)
myTown?.changePopulation(by: 1_000_000)
print("Size \(myTown?.townSize); Population: \(myTown?.population)")


var fredTheZombie: Zombie? = Zombie(limp: false, fallingApart: false, town: myTown, monsterName: "Fred")
fredTheZombie?.terrorizeTown()
fredTheZombie?.town?.printTownDescription()

var convientZombie = Zombie(limp: true, fallingApart: false)

let laloTheVampire = Vampire(town: fredTheZombie?.town, monsterName: "Lalo")
laloTheVampire.town = fredTheZombie?.town
laloTheVampire.terrorizeTown()
laloTheVampire.town?.printTownDescription()

//let arrayToCreate: [Monster.Type] = [Monster.self, Zombie.self, Vampire.self]
//let monster = arrayToCreate.map { monsterType in monsterType.init(town: nil, monsterName: "any")}



print("Victim pool: \(fredTheZombie?.victimPool)")
fredTheZombie?.victimPool = 500
print("Victim pool: \(fredTheZombie?.victimPool)")
print(Zombie.spookyNoise)
if Zombie.isTerrifying{
    print("Run away")
}
fredTheZombie = nil




