//
//  Zombie.swift
//  MonsterTown
//
//  Created by Héctor Moreno on 12/06/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation

class Zombie: Monster {
    static let isTerrifying = true
    override class var spookyNoise: String {
        return "Brainss..."
    }
    var walksWithLimp: Bool
    private(set) var isFallingApart: Bool
    
    init(limp: Bool, fallingApart: Bool, town: Town?, monsterName: String){
        walksWithLimp = limp
        isFallingApart = fallingApart
        super.init(town: town, monsterName: monsterName)
    }
    
    convenience init(limp: Bool, fallingApart: Bool) {
        self.init(limp: limp, fallingApart: fallingApart, town: nil, monsterName: "Freed")
        if walksWithLimp {
            print("This zombie has a bad knee")
        }
    }
    
    required init(town: Town?, monsterName: String) {
        walksWithLimp = false
        isFallingApart = false
        super.init(town: town, monsterName: monsterName)
    }
    
    deinit {
        print("Zombie named \(name) is no longer with us.")
    }
    
    final override func terrorizeTown() {
        if (town?.population)! >= 10 {
            if !isFallingApart{
                town?.changePopulation(by: -10)
            }
            super.terrorizeTown()
            
        } else {
            town?.population = 0
        }
    }
}
