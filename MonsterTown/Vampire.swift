//
//  File.swift
//  MonsterTown
//
//  Created by Héctor Moreno on 14/06/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation

class Vampire: Monster {
    var vampireThralls = [Int]()
    
    override func terrorizeTown() {
        vampireThralls.append(1)
        town?.changePopulation(by: -1)
        super.terrorizeTown()
    }
}
