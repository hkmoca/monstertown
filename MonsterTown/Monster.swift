//
//  Monster.swift
//  MonsterTown
//
//  Created by Héctor Moreno on 12/06/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
class Monster {
    var town: Town?
    var name: String?
    
    class var spookyNoise: String {
        return "Grr...."
    }
    
    var victimPool: Int {
        get {
            return town?.population ?? 0
        }
        set (newVictimPool) {
            town?.population = newVictimPool
        }
    }
    
    required init(town: Town?, monsterName: String){
        self.town = town
        name = monsterName
    }
    
    func terrorizeTown() {
        if town != nil {
            print("\(name) is terrorizing a town")
        } else {
            print("\(name) hasn't found a town to terrorize yet...")
        }
    }
}
